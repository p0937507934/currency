package route

import (
	"github.com/gin-gonic/gin"
)

func Serve() *gin.Engine {
	engine := gin.Default()
	CurrencyRoute(engine)
	return engine
}
