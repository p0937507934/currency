package currency

const (
	CURRENCY_TWD = "TWD"
	CURRENCY_JPY = "JPY"
	CURRENCY_USD = "USD"
)

var transformMap map[string]map[string]float64 = map[string]map[string]float64{
	CURRENCY_TWD: map[string]float64{
		CURRENCY_TWD: 1,
		CURRENCY_JPY: 3.669,
		CURRENCY_USD: 0.03281,
	},
	CURRENCY_JPY: map[string]float64{
		CURRENCY_TWD: 0.26956,
		CURRENCY_JPY: 1,
		CURRENCY_USD: 0.00885,
	},
	CURRENCY_USD: map[string]float64{
		CURRENCY_TWD: 30.444,
		CURRENCY_JPY: 111.801,
		CURRENCY_USD: 1,
	},
}
