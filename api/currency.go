package api

import (
	"currency/dto"
	"currency/pkg/response"
	"currency/service/currency"

	"github.com/gin-gonic/gin"
)

func GetTranformCurrency(c *gin.Context) {
	req := &dto.CurrencyTransformRequest{}
	if err := c.Bind(req); err != nil {
		res := response.NewResponse(struct{}{}, err)
		c.JSON(400, res)
		return
	}
	srv := currency.NewCurrencyService()
	res, err := srv.Transform(req)
	if err != nil {
		res := response.NewResponse(struct{}{}, err)
		c.JSON(400, res)
		return
	}

	c.JSON(200, response.NewResponse(res, nil))
}
