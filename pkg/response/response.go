package response

type Response struct {
	Response interface{} `json:"respones"`
	Error    string      `json:"error"`
}

func NewResponse(res interface{}, err error) *Response {
	var errString string
	if err != nil {
		errString = err.Error()
	}
	return &Response{Response: res, Error: errString}
}
