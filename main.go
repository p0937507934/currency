package main

import (
	"currency/route"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const (
	PORT = 8000
)

func main() {
	r := route.Serve()
	go func() {
		err := r.Run(fmt.Sprintf(":%d", PORT))
		if err != nil {
			panic(err)
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown...")
}
