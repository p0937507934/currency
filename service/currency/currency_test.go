package currency

import (
	"currency/dto"
	"errors"
	"fmt"
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestTransform(t *testing.T) {
	tests := []struct {
		Request     *dto.CurrencyTransformRequest
		Response    *dto.CurrencyTransformResponse
		ResponseErr error
		Name        string
	}{
		{
			Name: "Test TWD To JPY",
			Request: &dto.CurrencyTransformRequest{
				From:  CURRENCY_TWD,
				To:    CURRENCY_JPY,
				Value: 10000,
			},
			Response: &dto.CurrencyTransformResponse{
				Value: "36,690",
			},
		},
		{
			Name: "Test JPY To TWD",
			Request: &dto.CurrencyTransformRequest{
				From:  CURRENCY_JPY,
				To:    CURRENCY_TWD,
				Value: 10000,
			},
			Response: &dto.CurrencyTransformResponse{
				Value: "2,695.6",
			},
		},
		{
			Name: "Test TWD To USD",
			Request: &dto.CurrencyTransformRequest{
				From:  CURRENCY_TWD,
				To:    CURRENCY_USD,
				Value: 100000,
			},
			Response: &dto.CurrencyTransformResponse{
				Value: "3,281",
			},
		},
		{
			Name: "Test USD To TWD",
			Request: &dto.CurrencyTransformRequest{
				From:  CURRENCY_USD,
				To:    CURRENCY_TWD,
				Value: 100000,
			},
			Response: &dto.CurrencyTransformResponse{
				Value: "3,044,400",
			},
		},
		{
			Name: "Test JPY To USD",
			Request: &dto.CurrencyTransformRequest{
				From:  CURRENCY_JPY,
				To:    CURRENCY_USD,
				Value: 1000000,
			},
			Response: &dto.CurrencyTransformResponse{
				Value: "8,850",
			},
		},
		{
			Name: "Test USD To JPY",
			Request: &dto.CurrencyTransformRequest{
				From:  CURRENCY_USD,
				To:    CURRENCY_JPY,
				Value: 1000,
			},
			Response: &dto.CurrencyTransformResponse{
				Value: "111,801",
			},
		},
		{
			Name: "Test Currency Type error",
			Request: &dto.CurrencyTransformRequest{
				From:  "EUR",
				To:    CURRENCY_JPY,
				Value: 1000,
			},
			Response:    nil,
			ResponseErr: errors.New("request data parameter error"),
		},
	}
	srv := NewCurrencyService()
	for idx, test := range tests {
		t.Run(fmt.Sprintf("Test:%d", idx), func(t *testing.T) {
			res, err := srv.Transform(test.Request)
			assert.Equal(t, test.Response, res)
			assert.Equal(t, test.ResponseErr, err)
		})
	}
}
