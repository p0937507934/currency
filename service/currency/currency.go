package currency

import (
	"currency/dto"
	"errors"
	"math"

	"github.com/dustin/go-humanize"
)

type CurrencyService struct {
}

func NewCurrencyService() *CurrencyService {
	return &CurrencyService{}
}

func (s *CurrencyService) Transform(req *dto.CurrencyTransformRequest) (*dto.CurrencyTransformResponse, error) {
	if rate, ok := transformMap[req.From][req.To]; !ok {
		return nil, errors.New("request data parameter error")
	} else {
		result := math.Round((req.Value*rate)*100) / 100
		return &dto.CurrencyTransformResponse{Value: humanize.Commaf(float64(result))}, nil
	}
}
