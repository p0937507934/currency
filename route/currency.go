package route

import (
	"currency/api"

	"github.com/gin-gonic/gin"
)

func CurrencyRoute(r *gin.Engine) {
	r.GET("/currency", api.GetTranformCurrency)
}
