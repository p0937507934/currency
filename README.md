# Currency

Currency exchanger

## Getting started

```
    cd project_name
    <!-- 啟動伺服器 -->
    go run main.go
    <!-- 測試 -->
    go test ./...
```

## API Desciptions
設計一個 GET method, 根據參數回傳結果

1. /currency?from={from}&to={to}&value={number}


from: oneof -> TWD, JPY, USD (Case sensitive) (來源幣別)

to: oneof ->  TWD, JPY, USD (Case sensitive)  (目標幣別)

value: number  (兌換數量)

Response: 
value 為 轉換後的數量
若有 error 則顯示在 error


example: http://localhost:8000/currency?from=TWD&to=JPY&value=10000

## Project directory


api - 存放 api 

api_test - 放 integration test 

service - 1. 放相關服務功能及業務邏輯
          2. _test.go 結尾為對應的單元測試  

pkg - 小工具

dto - 定義 api 進來及出去的 request, response

route - api 路由

main - 主函數


