package apitest

import (
	"bytes"
	"currency/route"
	. "currency/service/currency"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
)

var routes *gin.Engine

func SetUp() {
	gin.SetMode(gin.TestMode)
	routes = route.Serve()
}

func TestTransformCurrency(t *testing.T) {
	SetUp()
	tests := []struct {
		From         string
		To           string
		Value        float64
		ExpectedCode int
		Name         string
	}{
		{
			From:         CURRENCY_TWD,
			To:           CURRENCY_JPY,
			Value:        10000,
			ExpectedCode: 200,
			Name:         "Test TWD TO JPY Succcess",
		},
		{
			From:         CURRENCY_TWD,
			To:           CURRENCY_USD,
			Value:        10000,
			ExpectedCode: 200,
			Name:         "Test TWD TO USD Succcess",
		},
		{
			From:         CURRENCY_JPY,
			To:           CURRENCY_USD,
			Value:        10000,
			ExpectedCode: 200,
			Name:         "Test JPY TO TWD Succcess",
		},
		{
			From:         CURRENCY_JPY,
			To:           CURRENCY_USD,
			Value:        10000,
			ExpectedCode: 200,
			Name:         "Test JPY TO USD Succcess",
		},
		{
			From:         CURRENCY_USD,
			To:           CURRENCY_TWD,
			Value:        10000,
			ExpectedCode: 200,
			Name:         "Test USD TO TWD Succcess",
		},
		{
			From:         CURRENCY_USD,
			To:           CURRENCY_JPY,
			Value:        10000,
			ExpectedCode: 200,
			Name:         "Test USD TO JPY Succcess",
		},
		{
			From:         "Unknown",
			To:           CURRENCY_TWD,
			Value:        10000,
			ExpectedCode: 400,
			Name:         "Test From unknown failed",
		},
		{
			From:         CURRENCY_TWD,
			To:           "Unknown",
			Value:        10000,
			ExpectedCode: 400,
			Name:         "Test To unknown failed",
		},
		{
			From:         "Unknown",
			To:           "Unknown",
			Value:        10000,
			ExpectedCode: 400,
			Name:         "Test  unknown failed",
		},
	}
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/currency?from=%s&to=%s&value=%f", test.From, test.To, test.Value), bytes.NewBuffer(nil))
			req.Header.Set("Content-Type", "application/json")
			response := httptest.NewRecorder()
			routes.ServeHTTP(response, req)
			assert.Equal(t, test.ExpectedCode, response.Code)
		})

	}

}
