package dto

type CurrencyTransformRequest struct {
	From  string  `json:"from" form:"from" binding:"required,oneof=TWD JPY USD"`
	To    string  `json:"To"  form:"to" binding:"required,oneof=TWD JPY USD"`
	Value float64 `json:"value"  form:"value" binding:"required,numeric"`
}

type CurrencyTransformResponse struct {
	Value string `json:"value"`
}
